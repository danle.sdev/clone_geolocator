package com.baseflow.geolocator.data;

import java.util.Map;

public class CoordinateOptions {

    public Coordinate coordinate;

    private CoordinateOptions(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @SuppressWarnings("ConstantConditions")
    public static CoordinateOptions parseArguments(Object arguments) {
        Coordinate coordinate;

        if (arguments == null) {
            throw new IllegalArgumentException("No coordinates supplied to calculate distance between.");
        }

        @SuppressWarnings("unchecked")
        Map<String, Double> coordinates = (Map<String, Double>) arguments;

        System.out.println(coordinates.toString());

        coordinate = new Coordinate(
                coordinates.get("latitude"),
                coordinates.get("longitude"));

        return new CoordinateOptions(coordinate);
    }
}
