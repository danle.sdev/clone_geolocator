package com.baseflow.geolocator.tasks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.baseflow.geolocator.data.CoordinateOptions;
import com.baseflow.geolocator.data.wrapper.ChannelResponse;
import com.baseflow.geolocator.utils.MainThreadDispatcher;

import io.flutter.plugin.common.PluginRegistry;

public class OpenMapWithDirectionTask extends Task<CoordinateOptions> {
    private final Context mContext;

    OpenMapWithDirectionTask(TaskContext<CoordinateOptions> context) {
        super(context);

        PluginRegistry.Registrar registrar = context.getRegistrar();

        mContext = registrar.activity() != null ? registrar.activity() : registrar.activeContext();
    }

    @Override
    public void startTask() {
        final ChannelResponse channelResponse = getTaskContext().getResult();

        final CoordinateOptions options = getTaskContext().getOptions();

        Uri navigationIntentUri = Uri.parse("google.navigation:q=" + options.coordinate.getLatitude() + "," + options.coordinate.getLongitude());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(mapIntent);
        } else {
            MainThreadDispatcher.dispatchError(
                    channelResponse,
                    "ERROR_OPEN_GOOGLE_MAPS",
                    "Unable to open google maps.",
                    null);
        }

    }
}
