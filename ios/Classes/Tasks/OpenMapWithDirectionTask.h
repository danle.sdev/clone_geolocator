//
//  OpenMapWithDirectionTask.h
//  clone_geolocator
//
//  Created by Dân Lê on 6/3/19.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Task.h"
#import "TaskProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface OpenMapWithDirectionTask : Task <TaskProtocol>

@end

NS_ASSUME_NONNULL_END
