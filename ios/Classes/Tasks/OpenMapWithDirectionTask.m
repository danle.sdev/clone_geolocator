//
//  OpenMapWithDirectionTask.m
//  clone_geolocator
//
//  Created by Dân Lê on 6/3/19.
//

#import "OpenMapWithDirectionTask.h"

@implementation OpenMapWithDirectionTask {
    CLLocation *_location;
}

- (instancetype)initWithContext:(TaskContext *)context completionHandler:(CompletionHandler)completionHandler {
    self = [super initWithContext:context completionHandler:completionHandler];
    if (self) {
        //
        [self parseCoordinates:context.arguments];
    }
    
    return self;
}

- (void)parseCoordinates:(id)arguments {
    if ([arguments isKindOfClass:[NSDictionary class]]) {
        CLLocationDegrees latitude = ((NSNumber *)arguments[@"latitude"]).doubleValue;
        CLLocationDegrees longitude = ((NSNumber *)arguments[@"longitude"]).doubleValue;
        
        _location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    } else {
        _location = nil;
    }
}

- (void)startTask {
    if (_location == nil) {
        [self handleErrorCode:@"ERROR_OPEN_GOOGLE_MAPS" message:@"Please supply coordinates."];
        [self stopTask];
        return;
    }
    

    
    //Open google map
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.google.com/maps/dir/?api=1&destination=%f,%f", _location.coordinate.latitude, _location.coordinate.longitude]]];
    
    [self stopTask];
}
@end
